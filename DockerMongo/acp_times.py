"""
Author: Alyssa Kelley

Disclaimer: I worked on this assignment with Anne Glickenhaus. No code was copied, but ideas
were shared. I also posted a reference link to a stack overflow post that helped me with 
shifting the minutes to create the final times. 

In hindsight, I see a lot of code repetition between the open_time function and the 
closed_time function so I would have seperated that shared code and put it in 
seperate functions.

Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow
import math

#  Note for CIS 322 Fall 2016:
#  You MUST provide the following two functions
#  with these signatures, so that I can write
#  automated tests for grading.  You must keep
#  these signatures even if you don't use all the
#  same arguments.  Arguments are explained in the
#  javadoc comments.
#


def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """

    # The calculation of opening time is based on the max speed. 
    max_speeds = [34.0, 32.0, 30.0, 28.0, 26.0] # this information was found here: https://rusa.org/pages/acp-brevet-control-times-calculator under the max speed column

    interval_num = 0 # There are only intervals 0-200, 200-400, 400-600, 600-1000, and 1000-1300 so this can never be more than 4 (because we are startig at 0 and not 1)
    total_time = 0 # This will keep track of the total time spent which we later convert to minutes to shift the start time
    km_distance = control_dist_km # We will be decrementing this value in the loop below while looping through the intervals so km_distance is a temp for the control distance

    # THE CODE BELOW CALCULATES THE TOTAL TIME IN HOURS:
    # This loop does not count for the last interval, which will be when km_distance is between 0-200 
    # The loop interval num cannot exceed 3 because we have 5 total intervals (0-4) and we go through 0-3 in this loop and evaluate the remaining interval below. This loops handles 200-400, 400-600, 600-1000, 1000-1300
    while (km_distance > 200 and interval_num < 3):
        curr_interval_time = (200 / max_speeds[interval_num]) # the units will be in HR
        total_time += curr_interval_time # adding to the total time
        interval_num += 1 # moving on to the next interval group
        km_distance -= 200 # removing the km from the interval we just counted the time for

    # This is accounting for the interval 0-200
    remaining_time_for_interval1 = (km_distance/max_speeds[interval_num])
    total_time += remaining_time_for_interval1

    # THE CODE BELOW CALCULATES THE TOTAL TIME IN MINUTES AND ADDS IT TO THE START TIME:

    # NOTE: I was originally calculating the total time in minutes by rounding up but causes some errors when running my test so a fellow
    # classmate shared with me that adding .5 fixed this issue so that is what I implemented. 
    total_time_in_minutes = int((total_time * 60) + .5)

    # Using the arrow functions to take the start time provided, and then shift it over the total amount of 
    # minutes we just caculated and keep it in the isoformat because that is the requested return type.
    # Used this stack overflow post to help learn how to increment the minutes using arrow: https://stackoverflow.com/questions/40590619/python-arrow-add-hour-minutes-etc-vs-replace
    complete_time_from_start = arrow.get(brevet_start_time).shift(minutes=total_time_in_minutes).isoformat()
    return complete_time_from_start
    # return arrow.now().isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    # The calculation on the closing time is based on the min speed.
    min_speeds = [15.0, 15.0, 15.0, 11.428, 13.333] # this information was found here: https://rusa.org/pages/acp-brevet-control-times-calculator under the max speed column

    # This IF statement handled the hour oddity that is explained in example 4 from the brevet documentation and tested 
    # by the test_close_oddities function in test_acp_times.py
    if control_dist_km == 0:
      closing_time = arrow.get(brevet_start_time).shift(minutes=60).isoformat() # Adds the hour
    else:
      # NOTE: the code below is basically the same as in opening times but using the min speeds.
      interval_num = 0 # There are only intervals 0-200, 200-400, 400-600, 600-1000, and 1000-1300 so this can never be more than 5
      total_time = 0 # this will keep track of the total time spent
      km_distance = control_dist_km
      while (km_distance > 200 and interval_num < 3):
        curr_interval_time = (200 / min_speeds[interval_num]) # the units will be hr
        total_time += curr_interval_time # adding to our total time
        interval_num += 1 # going to the next interval group
        km_distance -= 200 # removing the km from the interval we just counted the time for

      # accounting for the interval 0-200
      remaining_time_for_interval1 = (km_distance/min_speeds[interval_num])
      total_time += remaining_time_for_interval1

      # now needing to convert the time to minutes and add it to the start time like before
      # total_time_in_minutes = int(math.ceil(total_time * 60)) # total time is in hours, so to convert to minutes we multiply by 60 and then round up
      total_time_in_minutes = int((total_time * 60) + .5)
      closing_time = arrow.get(brevet_start_time).shift(minutes=total_time_in_minutes).isoformat()

    return closing_time
    # return arrow.now().isoformat()












