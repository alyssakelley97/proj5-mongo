import os
from flask import Flask, redirect, url_for, request, render_template, jsonify, session
from pymongo import MongoClient
import acp_times 
import logging
import arrow
import config
"""
Author: Alyssa Kelley

Diclaimer: I worked on this assignment with Anne Glickenhaus. No code was copied, but ideas
and logic was shared. 
"""

app = Flask(__name__)

client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb
db.tododb.delete_many({})

################################################################################## 
# The following chunk of code is directly from my flask_brevets.py file from proj4 
##################################################################################

import logging

###
# Globals
###
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

###
# Pages
###

@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    session['linkback'] = url_for("index")
    return render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))

    #linked to calc.html to get the brevet distance, begin time, and begin date as variables to use as pass to acp_times
    distance = request.args.get('distance', 0, type = int)
    start_time = request.args.get('start_time', "", type = str)
    start_date = request.args.get('start_date', "", type = str)
    print("This is the starting time: ", start_time)
    print("This is the starting date: ", start_date)
    print("THis is the distance: ", distance)

    # adding the date and time together and ensuring it is in the correct argument format to pass to the acp_times functions
    starting_time = arrow.get(start_date + " " + start_time).isoformat()
    print("Starting time = ", starting_time)

    open_time = acp_times.open_time(km, distance, starting_time)
    print("--> This is what open_time becomes = ", open_time)
    close_time = acp_times.close_time(km, distance, starting_time)
    print("--> This is what close_time becomes = ", close_time)
    
    # Displaying the open and close times as they already are formatted
    result = {"open": open_time, "close": close_time}
    return jsonify(result=result)
################################################################################## 

@app.route('/display', methods=['POST'])
def display():
    _items = db.tododb.find()
    items = [item for item in _items]

    if (len(items) == 0):
        return render_template('display_nothing_error.html')
    else:
        return render_template('todo.html', items=items)


@app.route('/new', methods=['POST'])
def new():
    # item_doc = {
    #     'name': request.form['name'],
    #     'description': request.form['description']
    # }
    # db.tododb.insert_one(item_doc)
    print("$$$$$$$$$$$ IN NEW $$$$$$$$$$$")
    info_for_open = request.form.getlist("open")
    info_for_close = request.form.getlist("close")
    info_for_km = request.form.getlist("km")
    info_length = 0

    open_times = []
    close_times = []
    kmvals = []

    for entry in info_for_open:
        if(str(entry) != ""):
            entry_str = str(entry)
            print(".... adding ", entry_str, " to open_times array")
            open_times.append(entry_str)

    for entry in info_for_close:
        if(entry):
            entry_str = str(entry)
            close_times.append(entry_str)

    for entry in info_for_km:
        if(entry):
            entry_str = str(entry)
            kmvals.append(entry_str)


    # print("**********This is the length of open times: ", len(open_times))
    # print("**********This is the length of close times: ", len(close_times))
    # print("**********This is the length of kms: ", len(kmvals))

    if ((len(open_times) == 0) or (len(close_times) == 0) or (len(kmvals) == 0)):
        return render_template('submit_nothing_error.html')

    info_length = (len(open_times))

    for curr_val in range(info_length):
        all_info = {
            'all_info_for_open': open_times[curr_val],
            'all_info_for_close': close_times[curr_val],
            'all_info_for_km': kmvals[curr_val],
        }
        db.tododb.insert_one(all_info)

    # When you click the submit button, you are not getting anything displayed, 
    # it is just taking you back to the calc.html page to enter new values
    return redirect(url_for('index'))
    # return redirect(url_for('todo'))

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
