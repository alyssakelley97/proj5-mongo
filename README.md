# Project 5: Brevet time calculator with Ajax and MongoDB

Author: Alyssa Kelley, alyssak@uoregon.edu

I worked on this assignment with Anne Glickenhaus. No code was copied, but ideas and logic were shared. 

## Functionality

This code is a continuation from project 4 seen here: https://alyssakelley97@bitbucket.org/alyssakelley97/proj4-brevets.git

I created a list of open and close controle times using AJAX

1) There are two buttons ("Submit") and ("Display") in the page where have controle times. 
2) When you click the Submit button, the control times are entered into the database. 
3) When you click the Display button, the entries from the database are displayed in a new page. 

The errors cases that are checked for these two buttons are when 1) submit is clicken when no controle times have been entered. This results in an error messages html page rendering and when 2) the display button is clicked but nothing has been entered into the database. This also results in an error message html page. Note: the database is cleared out at the beginning of the programs execution so you MUST enter values using the submit button before anything can be displayed.

Information regarding the concepts of the project
The algorithm for calculating controle times is described here (https://rusa.org/pages/acp-brevet-control-times-calculator). Additional background information is given here (https://rusa.org/pages/rulesForRiders).
This project is essentially replacing the calculator here (https://rusa.org/octime_acp.html).
Please see the proj4-brevets repository's README for a more in-depth description of the concepts and algorithms being used.